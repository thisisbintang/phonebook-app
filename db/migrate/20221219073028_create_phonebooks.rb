class CreatePhonebooks < ActiveRecord::Migration[7.0]
  def change
    create_table :phonebooks do |t|
      t.string :name, null: false
      t.string :email, null: false
      t.string :phone, null: false

      t.timestamps
    end
    add_index :phonebooks, :email, unique: true
    add_index :phonebooks, :phone, unique: true
  end
end
