10.times do |i|
    Phonebook.create(
        name: "User #{i + 1}",
        email: "user#{i + 1}@email.com",
        phone: "+628192828282#{i + 1}",
    )
end