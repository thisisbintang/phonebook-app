# Rails + React
Simple Phonebook App with Rails + React

## Features

- 🚈 [Ruby On Rails](https://rubyonrails.org) as REST API
- ⚡ Full-stack [React](https://reactjs.org/) with Rails
- 🧙‍♂️ Typesafety with [typescript](https://trpc.io)
- 🏠 Database with MySQL
- 🎨 Styling with [tailwindcss](https://tailwindcss.com/)

## Setup

```bash
rails db:create
rails db:migrate
rails db:seed
yarn
bin/dev
```
