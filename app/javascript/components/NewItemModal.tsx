import React, { useState } from 'react'
import { AiFillPhone } from 'react-icons/ai';
import { BiChevronLeft } from 'react-icons/bi';
import { BsFillPersonFill } from 'react-icons/bs';
import { MdEmail } from 'react-icons/md';
import Modal from 'react-modal'
import { IPhonebook } from '../screens/Home';

interface Props {
    isOpen: boolean;
    closeModal: () => void;
    onAddData: (data: IPhonebook) => void;
}

const NewItemModal: React.FC<Props> = ({ isOpen, closeModal, onAddData }) => {
    const [name, setName] = useState<string>('')
    const [email, setEmail] = useState<string>('')
    const [phone, setPhone] = useState<string>('')

    const handleSaveData = async () => {
        // if (!name || !email || !phone) {
        //     // alert('Data tidak lengkap')
        //     return
        // };

        const url = "/api/v1/phonebooks"
        const token = document.querySelector('meta[name="csrf-token"]').content;

        const body = {
            name,
            email,
            phone,
        };

        try {
            const response = await fetch(url, {
                method: 'POST',
                headers: {
                    "X-CSRF-Token": token,
                    "Content-Type": "application/json",
                },
                body: JSON.stringify(body)
            });
            const data = await response.json();
            console.log(data)
            if (!response.ok) {
                alert(data.message)
                return;
            }
            onAddData(data)
            setEmail('')
            setName('')
            setPhone('')
            closeModal();
        } catch (error) {
            if (error instanceof Error) {
                alert(error.message);
                return;
            }
            alert('Something wrong!')
        }
    }


    return (
        <Modal
            style={{
                content: {
                    top: '30%',
                    left: '50%',
                    right: 'auto',
                    bottom: 'auto',
                    marginRight: '-50%',
                    transform: 'translate(-50%, -50%)',
                }
            }}
            isOpen={isOpen}
            onRequestClose={closeModal}>
            <div className='bg-white container mx-auto rounded shadow-md p-6'>
                <div className="mb-4 flex items-center gap-x-3">
                    <div
                        onClick={closeModal}
                        className='cursor-pointer text-3xl text-black'>
                        <BiChevronLeft />
                    </div>
                    <h1>Tambah baru</h1>
                </div>

                <div className='flex flex-col gap-y-4'>
                    <div className='relative'>
                        <div className='absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none'>
                            <div className='absolute h-5 w-5'>
                                <BsFillPersonFill />
                            </div>
                        </div>
                        <input
                            onChange={(e) => setName(e.target.value)}
                            value={name}
                            type="text"
                            placeholder='Kylian Mbappé'
                            className='block text-base w-full pl-10 py-2 ring-1 ring-slate-900/10  rounded-lg' />
                    </div>
                    <div className='relative'>
                        <div className='absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none'>
                            <div className='absolute h-5 w-5'>
                                <MdEmail />
                            </div>
                        </div>
                        <input
                            onChange={(e) => setEmail(e.target.value)}
                            value={email}
                            type="email"
                            placeholder='kylianmbappé@psg.com'
                            className='block text-base w-full pl-10 py-2 ring-1 ring-slate-900/10 rounded-lg' />
                    </div>
                    <div className='relative'>
                        <div className='absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none'>
                            <div className='absolute h-5 w-5'>
                                <AiFillPhone />
                            </div>
                        </div>
                        <input
                            onChange={(e) => setPhone(e.target.value)}
                            value={phone}
                            type="text"
                            placeholder='08123789456'
                            className='block text-base w-full pl-10 py-2 ring-1 ring-slate-900/10  rounded-lg' />
                    </div>
                    <button
                        onClick={handleSaveData}
                        className='bg-green-500 rounded-md py-2 text-white'>simpan</button>
                </div>
            </div>
        </Modal>
    )
}

export default NewItemModal