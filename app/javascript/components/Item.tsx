import React, { useState } from 'react'
import { BiPencil, BiTrash } from 'react-icons/bi';
import { IPhonebook } from '../screens/Home'
import EditItemModal from './EditItemModal';

interface Props {
    phonebook: IPhonebook
    onDeleteItem: (id: number) => void;
    onUpdateItem: (phonebook: IPhonebook) => void;
}

const Item: React.FC<Props> = (props) => {
    const { phonebook, onDeleteItem, onUpdateItem } = props;
    const [showEditModal, setShowEditModal] = useState(false)

    const handleCloseEditModal = () => {
        setShowEditModal(false)
    }

    const handleDeleteData = async () => {
        const url = `/api/v1/phonebooks/${phonebook.id}`
        const token = document.querySelector('meta[name="csrf-token"]').content;

        try {
            const response = await fetch(url, {
                method: 'DELETE',
                headers: {
                    "X-CSRF-Token": token,
                    "Content-Type": "application/json",
                },
            })
            const data = await response.json();
            if (!response.ok) {
                alert(data.message)
                return;
            }
            onDeleteItem(phonebook.id)
        } catch (error) {
            console.log("Something wrong!")
        }
    }

    return (
        <>
            <div
                className="bg-slate-50 rounded shadow-sm flex justify-between items-center px-2 py-1">
                <div className="flex items-baseline gap-x-2">
                    <h3 className="text-base">
                        {phonebook.name}
                    </h3>
                    <div className="bg-slate-100 border border-slate-300 rounded-full px-2 text-xs">{phonebook.email}</div>
                    <div className="bg-lime-100 border border-lime-300 rounded-full px-2 text-xs">{phonebook.phone}</div>
                </div>
                <div className="flex items-center gap-x-2">
                    <button
                        onClick={() => {
                            setShowEditModal(true)
                        }}
                        className=" bg-yellow-500 rounded-sm shadow-md p-1 text-white">
                        <BiPencil />
                    </button>
                    <button
                        onClick={() => handleDeleteData()}
                        className=" bg-red-500 slate-50 rounded-sm shadow-md p-1 text-white">
                        <BiTrash />
                    </button>
                </div>

            </div>
            <EditItemModal
                onUpdateItem={onUpdateItem}
                closeModal={handleCloseEditModal}
                isOpen={showEditModal}
                data={phonebook}
            />
        </>
    )
}

export default Item