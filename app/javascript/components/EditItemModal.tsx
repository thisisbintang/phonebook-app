import React, { useEffect, useState } from 'react'
import { AiFillPhone } from 'react-icons/ai';
import { BiChevronLeft } from 'react-icons/bi';
import { BsFillPersonFill } from 'react-icons/bs';
import { MdEmail } from 'react-icons/md';
import Modal from 'react-modal'
import { IPhonebook } from '../screens/Home';

interface Props {
    data: IPhonebook
    isOpen: boolean;
    closeModal: () => void;
    onUpdateItem: (phonebook: IPhonebook) => void;
}

const EditItemModal: React.FC<Props> = (props) => {
    const { closeModal, data, isOpen, onUpdateItem } = props;
    const [name, setName] = useState<string>(data.name)
    const [email, setEmail] = useState<string>(data.email)
    const [phone, setPhone] = useState<string>(data.phone)

    const handleSaveData = async () => {
        // if (!name || !email || !phone) return;

        const url = `/api/v1/phonebooks/${data.id}`
        const token = document.querySelector('meta[name="csrf-token"]').content;

        const body = {
            name,
            email,
            phone,
        };

        try {
            const response = await fetch(url, {
                method: 'PATCH',
                headers: {
                    "X-CSRF-Token": token,
                    "Content-Type": "application/json",
                },
                body: JSON.stringify(body)
            });

            const json = await response.json();
            if (response.ok) {
                onUpdateItem({ name, email, phone, id: data.id })
                closeModal();
                return;
            }
            alert((json as any).message)
        } catch (error) {
            alert('Something wrong!')
        }
    }

    return (
        <Modal
            style={{
                content: {
                    top: '30%',
                    left: '50%',
                    right: 'auto',
                    bottom: 'auto',
                    marginRight: '-50%',
                    transform: 'translate(-50%, -50%)',
                }
            }}
            isOpen={isOpen}
            onRequestClose={() =>{
                closeModal()
                setEmail(data.email)
                setName(data.name)
                setPhone(data.phone)
            }}>
            <div className='bg-white container mx-auto rounded shadow-md p-6'>
                <div className="mb-4 flex items-center gap-x-3">
                    <div
                        onClick={closeModal}
                        className='cursor-pointer text-3xl text-black'>
                        <BiChevronLeft />
                    </div>
                    <h1>Ubah data</h1>
                </div>

                <div className='flex flex-col gap-y-4'>
                    <div className='relative'>
                        <div className='absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none'>
                            <div className='absolute h-5 w-5'>
                                <BsFillPersonFill />
                            </div>
                        </div>
                        <input
                            onChange={(e) => setName(e.target.value)}
                            value={name}
                            type="text"
                            placeholder='Kylian Mbappé'
                            className='block text-base w-full pl-10 py-2 ring-1 ring-slate-900/10 rounded-lg' />
                    </div>
                    <div className='relative'>
                        <div className='absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none'>
                            <div className='absolute h-5 w-5'>
                                <MdEmail />
                            </div>
                        </div>
                        <input
                            onChange={(e) => setEmail(e.target.value)}
                            value={email}
                            type="email"
                            placeholder='kylianmbappé@psg.com'
                            className='block text-base w-full pl-10 py-2 ring-1 ring-slate-900/10  rounded-lg' />
                    </div>
                    <div className='relative'>
                        <div className='absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none'>
                            <div className='absolute h-5 w-5'>
                                <AiFillPhone />
                            </div>
                        </div>
                        <input
                            onChange={(e) => setPhone(e.target.value)}
                            value={phone}
                            type="text"
                            placeholder='08123789456'
                            className='block text-base w-full pl-10 py-2 ring-1 ring-slate-900/10 rounded-lg' />
                    </div>
                    <button
                        onClick={handleSaveData}
                        className='bg-green-500 rounded-md py-2 text-white'>simpan</button>
                </div>


            </div>
        </Modal>
    )
}

export default EditItemModal