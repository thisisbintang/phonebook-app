import React, { useEffect, useState } from "react";
import { BiPlus } from 'react-icons/bi'
import Item from "../components/Item";
import NewItemModal from "../components/NewItemModal";
import { useAutoAnimate } from '@formkit/auto-animate/react'

export interface IPhonebook {
  id: number;
  name: string;
  email: string;
  phone: string;
}

const Home: React.FC = () => {
  const [phonebooks, setPhonebooks] = useState<IPhonebook[]>([]);
  const [showNewModal, setShowNewModal] = useState(false);
  const [listItems] = useAutoAnimate()

  useEffect(() => {
    const url = "/api/v1/phonebooks"

    const fetchPhonebooks = async () => {
      try {
        const response = await fetch(url);
        const data = await response.json();
        setPhonebooks(data);
      } catch (error) {
        throw Error("Something wrong!")
      }
    }

    fetchPhonebooks();
  }, []);

  const handleCloseNewModal = () => {
    setShowNewModal(false)
  }

  return (
    <>
      <div className="bg-white container mx-auto rounded shadow-md p-6">
        <div className="mb-4 flex items-center gap-x-3">
          <h1 className="text-3xl font-semibold">Phonebook Apps</h1>
          <button
            onClick={() => setShowNewModal(true)}
            className="bg-green-500 rounded-md flex items-center p-1 px-2 gap-x-2 text-white text-sm"><BiPlus /> tambah baru</button>
        </div>
        <div
          ref={listItems}
          className="flex flex-col gap-y-4">
          {
            phonebooks.length > 0 ? phonebooks.map(phonebook => (
              <Item
                key={phonebook.id}
                phonebook={phonebook}
                onUpdateItem={(phonebook) => {
                  setPhonebooks(prevValue => {
                    const currentValue = [...prevValue]
                    const indexItem = currentValue.findIndex(item => item.id === phonebook.id)
                    if (indexItem !== -1) currentValue.splice(indexItem, 1, phonebook)
                    return currentValue;
                  })
                }}
                onDeleteItem={(id) => setPhonebooks(prevValue => prevValue?.filter(item => item.id !== id))}
              />
            )) : (
              <div className="text-center">
                <p>No item found</p>
              </div>
            )
          }
        </div>
      </div>
      <NewItemModal
        onAddData={(data) => setPhonebooks(prev => [...prev, data])}
        isOpen={showNewModal}
        closeModal={handleCloseNewModal}
      />
      
    </>
  )
}

export default Home;