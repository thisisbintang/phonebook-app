import React from "react";
import { createRoot } from "react-dom/client";
import App from "./App";
import Modal from 'react-modal';

document.addEventListener("turbo:load", () => {
  const appElement = document.createElement("div");

  Modal.setAppElement(appElement);

  const root = createRoot(
    document.body.appendChild(appElement)
  );

  root.render(<App />);
});