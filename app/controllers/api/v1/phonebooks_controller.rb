class Api::V1::PhonebooksController < ApplicationController
  before_action :set_phonebook, only: %i[show update destroy]
  
  def index
    phonebooks = Phonebook.all
    render json: phonebooks
  end

  def create
    begin
      phonebook = Phonebook.create!(phonebook_params)
      render json: phonebook
    rescue => exception
      render json: { message: exception.to_s }, status: 500
    end
  end

  def show
    render json: @phonebook
  end

  def update
    begin
      updatedData = @phonebook.update!(phonebook_params)
      render json: updatedData
    rescue => exception
      render json: { message: exception.to_s }, status: 500
    end
  end

  def destroy
    begin
      @phonebook.destroy!
      render json: @phonebook
    rescue => exception
      render json: { message: exception.to_s }, status: 500
    end
  end

  private

  def phonebook_params
    params.require(:phonebook).permit(:name, :email, :phone)
  end

  def set_phonebook
    @phonebook = Phonebook.find_by_id(params[:id])
    if @phonebook == nil
      render json: { message: "Data not found " }, status: 404
    end
  end
end
